#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPClient.h>
#include <ArduinoUniqueID.h>
#include <ArduinoJson.h>
#include <FastLED.h>

ESP8266WebServer server(80);

// How many leds in your strip?
#define NUM_LEDS 16
#define DATA_PIN D6
CRGB leds[NUM_LEDS];

const char* ssid = ""; //Enter Wi-Fi SSID
const char* password =  ""; //Enter Wi-Fi Password
char device_id[16];

void(* resetFunc) (void) = 0;

////////////////////////////////////////////////////////////////////////////////

void init_wifi() {
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
 
  // Connect to the WiFi network
  WiFi.begin(ssid, password);

  Serial.print("Waiting to connect");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.print(".");
  }
  Serial.println("");

  // Print the local IP
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

////////////////////////////////////////////////////////////////////////////////

void array_to_string(byte array[], unsigned int len, char buffer[]) {
    for (unsigned int i = 0; i < len; i++)
    {
        byte nib1 = (array[i] >> 4) & 0x0F;
        byte nib2 = (array[i] >> 0) & 0x0F;
        buffer[i*2+0] = nib1  < 0xA ? '0' + nib1  : 'A' + nib1  - 0xA;
        buffer[i*2+1] = nib2  < 0xA ? '0' + nib2  : 'A' + nib2  - 0xA;
    }
    buffer[len*2] = '\0';
}

////////////////////////////////////////////////////////////////////////////////

void print_device_id() {
  array_to_string(UniqueID, UniqueIDsize, device_id);
  Serial.print("Device ID: 0x");
  Serial.println(device_id);
}

////////////////////////////////////////////////////////////////////////////////

void setup() {
  Serial.begin(115200); //Begin Serial at 115200 Baud
  delay(500); // Let serial finish initializing before sending data to it

  Serial.println("--------------------------------------------------------------------------------");
  Serial.println("Initializing...");

  print_device_id();

  init_wifi();

  server.on("/",       HTTP_GET,  handle_index);
  server.on("/reboot", HTTP_POST, handle_reboot);
  server.on("/color",  HTTP_POST, handle_color);
  
  server.begin(); //Start the server
  Serial.println("Server listening");

  init_leds();
  delay(500);
  set_hex_color("0xff0000");
}

////////////////////////////////////////////////////////////////////////////////

void loop() {
  server.handleClient(); //Handling of incoming client requests
}

////////////////////////////////////////////////////////////////////////////////

void blink_me(int pin) {
  digitalWrite(pin, HIGH);
  delay(1000);
  digitalWrite(pin, LOW);
  delay(1000); 
}

////////////////////////////////////////////////////////////////////////////////

void handle_index() {
  server.send(200, "text/plain", "Use the REST API /color to set the Christmas tree LED color\n");
}

////////////////////////////////////////////////////////////////////////////////

void handle_reboot() {
  server.send(201, "text/plain", "rebooting...");
  reboot();
}

////////////////////////////////////////////////////////////////////////////////

void handle_color() {
  String message = "";

  if (server.arg("rgb")== "") {
    // Parameter not found
    message = "rgb not specified";
  } else {
    // Parameter found
    // Get the value of 'rgb'
    message += "0x";
    message += server.arg("rgb");

    set_hex_color(message.c_str());
  }

  // Return the HTTP response
  server.send(201, "text/plain", message);
}

void set_hex_color(const char* hex) {
  // Convert hex-string to unsigned integer
  int color = strtol(hex, NULL, 0);
  set_led_color(color);
}

////////////////////////////////////////////////////////////////////////////////

void set_led_color(int color) {
  for (int i = 0; i < NUM_LEDS; i++) {
    leds[i] = color;
    FastLED.show();
  }
}

////////////////////////////////////////////////////////////////////////////////

void reboot() {
  Serial.println("Rebooting...");
  delay(500);
  resetFunc();
}

////////////////////////////////////////////////////////////////////////////////

void init_leds() {
  FastLED.addLeds<WS2812B, DATA_PIN, RGB>(leds, NUM_LEDS);
  FastLED.show();
}

